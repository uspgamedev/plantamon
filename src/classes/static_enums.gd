 extends Node

enum SpeedModifier {SLOWER, NORMAL, FASTER}
enum MoveType {PHYSICAL, SPECIAL, STATUS, SWAP, SWITCH}
enum MoveElement {SUN, DEW, MINERAL, ION, SKY}
enum MovePosition {FRONT, BACK, BOTH}
