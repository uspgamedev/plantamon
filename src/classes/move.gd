class_name Move
extends Node

signal message(text)

export(StaticEnums.MovePosition) var launch_position;
export(StaticEnums.MovePosition) var hit_position;
export(StaticEnums.MoveType) var move_type;
export(StaticEnums.MoveElement) var move_element;
export(int) var power;

func _calculate_multiplier(attacker : Plantamon, defender : Plantamon) -> float:
	var multiplier : float = 1.0;
	match(self.move_element):
		StaticEnums.MoveElement.SUN:
			multiplier += (attacker.sun + defender.sky - defender.dew) * 0.2;
		StaticEnums.MoveElement.DEW:
			multiplier += (attacker.dew + defender.sun - defender.ion) * 0.2;
		StaticEnums.MoveElement.ION:
			multiplier += (attacker.ion + defender.dew - defender.mineral) * 0.2;
		StaticEnums.MoveElement.MINERAL:
			multiplier += (attacker.mineral + defender.ion - defender.sky) * 0.2;
		StaticEnums.MoveElement.SKY:
			multiplier += (attacker.sky + defender.mineral - defender.sun) * 0.2;
	return multiplier;

func _calculate_attr_total(attacker : Plantamon, defender : Plantamon) -> int:
	var attr_total : int = 0;
	if(self.move_type == StaticEnums.MoveType.PHYSICAL):
		attr_total = attacker.strengh - defender.defense;
	else:
		attr_total = attacker.mind - defender.willpower;
	return attr_total;

func _calculate_damage(multiplier : float, attr_total : int) -> int:
	var damage : int = ceil(attr_total * self.power * multiplier);
	return damage;

func _apply_bonus_effect(attacker : Plantamon, defender : Plantamon):
	self.emit_signal("message", attacker.plant_name + " attacked " + defender.plant_name + " with " + self.name);

func apply_move(attacker : Plantamon, defender : Plantamon):
	if(self.move_type == StaticEnums.MoveType.PHYSICAL \
			or self.move_type == StaticEnums.MoveType.SPECIAL):
		var multiplier : float = _calculate_multiplier(attacker, defender);
		var attr_total : int  = _calculate_attr_total(attacker, defender);
		var damage : int = _calculate_damage(multiplier, attr_total);
		if(damage > 0):
			defender.hp -= damage;
	_apply_bonus_effect(attacker, defender);
