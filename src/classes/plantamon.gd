class_name Plantamon
extends Polygon2D

export(String) var plant_name : String;

export(int) var max_hp : int;
export(int) var strengh : int;
export(int) var defense : int;
export(int) var mind : int;
export(int) var willpower : int;
export(int) var speed : int;

# Maybe change this to enum DICT
export(int) var sun : int;
export(int) var dew : int;
export(int) var mineral : int;
export(int) var ion : int;
export(int) var sky : int;

export(int) var hp : int;
export(Array, PackedScene) var moves_scene : Array;
var moves : Array;
var on_front : bool;
var speed_modifier : int;

func _ready():
	if(plant_name == null 
			or max_hp == null 
			or strengh == null
			or defense == null
			or mind == null
			or willpower == null
			or speed == null
			or sun == null
			or dew == null
			or mineral == null
			or ion == null
			or sky == null
		):
		assert(false, "Algum atributo do plantamon está nulo!")
	for move_scene in moves_scene:
		moves.append(move_scene.instance())
	hp = max_hp;
	on_front = true;
	speed_modifier = StaticEnums.SpeedModifier.NORMAL;

