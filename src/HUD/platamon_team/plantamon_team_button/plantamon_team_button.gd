extends Button

signal plantamon_selected(plantamon)

var plantamon : Plantamon setget _set_plantamon;

func _set_plantamon(new_planta : Plantamon):
	plantamon = new_planta
	self.text = plantamon.plant_name + " : " + str(plantamon.hp)

func update_text():
	self.text = plantamon.plant_name + " : " + str(plantamon.hp)

func _on_pressed():
	self.emit_signal("plantamon_selected", plantamon, self)
