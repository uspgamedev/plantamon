extends VBoxContainer

signal plantamon_selected(plantamon)

export(PackedScene) var button_scene : PackedScene;
var display : Array setget _set_display;

func _set_display(team : Array):
	display = team

	for button in self.get_children():
		button.disconnect("plantamon_selected", self, "_on_plantamon_selected")
		self.call_deferred("remove_child", button)
	yield(get_tree(), "idle_frame")

	for plantamon in display:
		var button = button_scene.instance();
		button.plantamon = plantamon
		button.call_deferred("connect", "plantamon_selected", self, "_on_plantamon_selected")
		self.call_deferred("add_child", button);
	yield(get_tree(), "idle_frame");

func _on_plantamon_selected(plantamon : Plantamon, selected_button : Button):
	for button in self.get_children():
		button.disabled = false;
		button.update_text();
	selected_button.disabled = true
	emit_signal("plantamon_selected", plantamon)
