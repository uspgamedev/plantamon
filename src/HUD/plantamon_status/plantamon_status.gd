extends Label

var display : Plantamon

func _process(_delta):
	if(display != null):
		self.text = "Name: " + display.plant_name \
				+ "\nHP: " + str(display.hp) \
				+ "\nSun: " + str(display.sun) \
				+ "\nDew: " + str(display.dew) \
				+ "\nIon: " + str(display.ion) \
				+ "\nMineral: " + str(display.mineral) \
				+ "\nSky: " + str(display.sky)
