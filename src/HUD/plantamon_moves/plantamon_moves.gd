extends VBoxContainer

signal move_selected(move)

export(PackedScene) var button_scene : PackedScene
var display : Plantamon setget _set_display

func _set_display(planta : Plantamon):
	display = planta

	for button in self.get_children():
		button.disconnect("move_selected", self, "_on_move_selected")
		self.call_deferred("remove_child", button)
	yield(get_tree(), "idle_frame")

	for move in display.moves:
		var button = button_scene.instance();
		button.move = move
		button.call_deferred("connect", "move_selected", self, "_on_move_selected")
		self.call_deferred("add_child", button);
	yield(get_tree(), "idle_frame");

func _on_move_selected(move : Move):
	self.emit_signal("move_selected", move)
