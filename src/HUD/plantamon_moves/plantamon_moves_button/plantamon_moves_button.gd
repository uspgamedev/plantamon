extends Button

signal move_selected(move)

var move : Move setget _set_move;

func _set_move(new_move : Move):
	move = new_move
	self.text = move.name

func _on_pressed():
	self.emit_signal("move_selected", move)
