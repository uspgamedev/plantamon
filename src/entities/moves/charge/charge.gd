extends Move

func _apply_bonus_effect(attacker : Plantamon, _defender : Plantamon):
	attacker.ion += 2;
	self.emit_signal("message", attacker.plant_name + " charged two ions");
