extends Move

func _apply_bonus_effect(attacker : Plantamon, _defender : Plantamon):
	attacker.sun += 1;
	attacker.speed_modifier = StaticEnums.SpeedModifier.FASTER
	self.emit_signal("message", attacker.plant_name + " charged one sun.");
	self.emit_signal("message", attacker.plant_name + " is faster!");
