extends Move

func _apply_bonus_effect(attacker : Plantamon, _defender : Plantamon):
	attacker.speed_modifier = StaticEnums.SpeedModifier.FASTER;
	self.emit_signal("message", attacker.plant_name + " switched in!");
