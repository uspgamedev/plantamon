extends Move

func _apply_bonus_effect(attacker : Plantamon, _defender : Plantamon):
	attacker.on_front = !attacker.on_front;
	attacker.speed_modifier = StaticEnums.SpeedModifier.FASTER;
	self.emit_signal("message", attacker.plant_name + " moved around the battlefield");
