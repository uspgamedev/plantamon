extends Node2D

signal plantamon_selected()

enum States {ENTRY, PLAYING, ONE_SELECTING, TWO_SELECTING}

export(Array, PackedScene) var team_one_scenes : Array;
export(Array, PackedScene) var team_two_scenes : Array;

var team_one : Array;
var team_two : Array;

var curr_state : int;

func _ready():
	for planta in team_one_scenes:
		team_one.append(planta.instance());

	for planta in team_two_scenes:
		team_two.append(planta.instance());

	$Overlay/TeamOneOverlay/TeamOneSelector.display = team_one;
	$Overlay/TeamTwoOverlay/TeamTwoSelector.display = team_two;

	curr_state = States.ENTRY;
	$CombatManager.team_manager = self

func _on_TeamOne_selected(plantamon : Plantamon):
	if(curr_state == States.ENTRY):
		$CombatManager.team_one = plantamon
	elif(curr_state == States.ONE_SELECTING):
		$CombatManager.team_one_switch = plantamon
		$Overlay/TeamOneOverlay.hide();
		curr_state = States.PLAYING;
		emit_signal("plantamon_selected", plantamon)

func _on_TeamTwo_selected(plantamon : Plantamon):
	if(curr_state == States.ENTRY):
		$CombatManager.team_two = plantamon
	elif(curr_state == States.TWO_SELECTING):
		$CombatManager.team_two_switch = plantamon
		$Overlay/TeamTwoOverlay.hide();
		curr_state = States.PLAYING;
		emit_signal("plantamon_selected", plantamon)

func _on_combat_started():
	$Overlay/TeamOneOverlay.hide();
	$Overlay/TeamTwoOverlay.hide();
	curr_state = States.PLAYING;

func _on_switch_in(teamOne):
	# This will lead to problems if both teams try to select at the same time
	if(teamOne):
		$Overlay/TeamOneOverlay.show();
		self.curr_state = States.ONE_SELECTING
	else:
		$Overlay/TeamTwoOverlay.show();
		self.curr_state = States.TWO_SELECTING
