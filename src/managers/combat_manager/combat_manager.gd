extends Node2D

signal message(text)
signal combat_started()
signal switch_in(teamOne)

enum States {ENTRY, WAITING_FOR_MOVES, CALCULATING, FIRST_MOVE, SECOND_MOVE, TURN_END};
var curr_state : int;

export(PackedScene) var swap_move_scene : PackedScene;
export(PackedScene) var switch_move_scene : PackedScene;

export(PackedScene) var debug_team_one : PackedScene;
export(PackedScene) var debug_team_two : PackedScene;

var team_one : Plantamon setget _set_team_one;
var team_two : Plantamon setget _set_team_two;

var team_one_switch : Plantamon;
var team_two_switch : Plantamon;

var team_one_move : Move;
var team_two_move : Move;

var one_is_first : bool;
var swap_move : Move;
var switch_move : Move;

var team_manager : Node2D;

func _set_team_one(planta : Plantamon):
	if(curr_state != States.ENTRY):
		planta.on_front = team_one.on_front
		for move in team_one.moves:
			move.disconnect("message", $Screen/TeamMoves/BattleLog, "log_message")
		$Plantas.call_deferred("remove_child", team_one)
		emit_signal("message",
			team_one.plant_name + " was switched with " + planta.plant_name)
		yield(get_tree(), "idle_frame")

	team_one = planta
	$Plantas.call_deferred("add_child", team_one);
	yield(get_tree(), "idle_frame")

	for move in team_one.moves:
		move.connect("message", $Screen/TeamMoves/BattleLog, "log_message")
	$Screen/HUD/StatusOne.display = team_one;
	$Screen/HUD/MovesOne.display = team_one;

	if(curr_state != States.ENTRY):
		_update_battlefield()

func _set_team_two(planta : Plantamon):
	if(curr_state != States.ENTRY):
		planta.on_front = team_two.on_front
		for move in team_two.moves:
			move.disconnect("message", $Screen/TeamMoves/BattleLog, "log_message")
		$Plantas.call_deferred("remove_child", team_two)
		emit_signal("message",
			team_two.plant_name + " was switched with " + planta.plant_name)
		yield(get_tree(), "idle_frame")

	team_two = planta
	$Plantas.call_deferred("add_child", team_two);
	yield(get_tree(), "idle_frame")

	for move in team_two.moves:
		move.connect("message", $Screen/TeamMoves/BattleLog, "log_message")
	$Screen/HUD/StatusTwo.display = team_two;
	$Screen/HUD/MovesTwo.display = team_two;

	if(curr_state != States.ENTRY):
		_update_battlefield()

func _on_MovesOne_move_selected(move):
	if(curr_state == States.WAITING_FOR_MOVES):
		team_one_move = move

func _on_MovesTwo_move_selected(move):
	if(curr_state == States.WAITING_FOR_MOVES):
		team_two_move = move

func _on_OneSwap_pressed():
	team_one_move = swap_move

func _on_TwoSwap_pressed():
	team_two_move = swap_move

func _on_OneSwitch_pressed():
	emit_signal("switch_in", true)
	yield(team_manager, "plantamon_selected")
	team_one_move = switch_move

func _on_TwoSwitch_pressed():
	emit_signal("switch_in", false)
	yield(team_manager, "plantamon_selected")
	team_two_move = switch_move

func _update_battlefield():
	if(team_one.on_front):
		team_one.global_position = $Screen/Combat/One_Front/Center.get_global_position();
	else:
		team_one.global_position = $Screen/Combat/One_Back/Center.get_global_position();

	if(team_two.on_front):
		team_two.global_position = $Screen/Combat/Two_Front/Center.get_global_position();
	else:
		team_two.global_position = $Screen/Combat/Two_Back/Center.get_global_position();

func _calculate_speed(plantamon : Plantamon) -> int:
	var speed : int = plantamon.speed;
	match(plantamon.speed_modifier):
		StaticEnums.SpeedModifier.FASTER:
			speed *= 2;
		StaticEnums.SpeedModifier.SLOWER:
			speed = ceil(0.5*speed);
	plantamon.speed_modifier = StaticEnums.SpeedModifier.NORMAL;
	return speed;

func _verify_move(one_first):
	var move : Move = team_one_move;
	var attacker : Plantamon =  team_one;
	var defender : Plantamon =  team_two;
	if(not one_first):
		move = team_two_move;
		attacker = team_two;
		defender = team_one;

	match(move.launch_position):
		StaticEnums.MovePosition.FRONT:
			if(not attacker.on_front):
				emit_signal("message",
						attacker.plant_name + " used a front launch move while being in the back!")
				return;

		StaticEnums.MovePosition.BACK:
			if(attacker.on_front):
				emit_signal("message",
						attacker.plant_name + " used a back launch move while being in the front!")
				return;

	match(move.hit_position):
		StaticEnums.MovePosition.FRONT:
			if(not defender.on_front):
				emit_signal("message",
						attacker.plant_name + "'s attack missed!")
				return;

		StaticEnums.MovePosition.BACK:
			if(defender.on_front):
				emit_signal("message",
						attacker.plant_name + "'s attack missed!")
				return;

	if(move.move_type == StaticEnums.MoveType.SWITCH):
		if(one_first):
			self.team_one = team_one_switch
		else:
			self.team_two = team_two_switch

	move.apply_move(attacker, defender)
	_update_battlefield();

func _ready():
	assert(swap_move_scene != null)
	swap_move = swap_move_scene.instance();
	swap_move.connect("message", $Screen/TeamMoves/BattleLog, "log_message")

	assert(switch_move_scene != null)
	switch_move = switch_move_scene.instance();
	switch_move.connect("message", $Screen/TeamMoves/BattleLog, "log_message")

	curr_state = States.ENTRY;

	if(not debug_team_one == null and not debug_team_two == null):
		self.team_one = debug_team_one.instance();
		self.team_two = debug_team_two.instance();

func _physics_process(_delta):
	match(curr_state):
		States.ENTRY:
			if(team_one != null and team_two != null):
				curr_state = States.WAITING_FOR_MOVES;
				_update_battlefield();
				self.emit_signal("combat_started")

		States.WAITING_FOR_MOVES:
			if(team_one_move != null and team_two_move != null):
				curr_state = States.CALCULATING;

		States.CALCULATING:
			var one_speed = _calculate_speed(team_one)
			var two_speed = _calculate_speed(team_two)
			if(one_speed > two_speed):
				one_is_first = true;
			elif(two_speed > one_speed):
				one_is_first = false;
			else:
				one_is_first = rand_range(0, 1) > 0.5;
			curr_state = States.FIRST_MOVE;

		States.FIRST_MOVE:
			_verify_move(one_is_first);
			curr_state = States.SECOND_MOVE;

		States.SECOND_MOVE:
			_verify_move(not one_is_first);
			curr_state = States.TURN_END;

		States.TURN_END:
			team_one_move = null;
			team_two_move = null;
			curr_state = States.WAITING_FOR_MOVES
